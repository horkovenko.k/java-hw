package sample;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static private String[][] schedule = new String[7][2];

    public static void main(String[] args) {

        schedule[0][0] = "Sunday";
        schedule[0][1] = "Sunday task";
        schedule[1][0] = "Monday";
        schedule[1][1] = "Monday task";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "Tuesday task";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "Wednesday task";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "Thursday task";
        schedule[5][0] = "Friday";
        schedule[5][1] = "Friday task";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "Saturday task";

        Scanner myObj = new Scanner(System.in);

        boolean isProgramWorking = true;

        while (isProgramWorking) {
            System.out.println("Please, input the day of the week: ");

            String userAnswer = myObj.nextLine().toLowerCase().trim();

            switch (userAnswer) {
                case "sunday":
                    System.out.println(schedule[0][1]);
                    break;
                case "monday":
                    System.out.println(schedule[1][1]);
                    break;
                case "tuesday":
                    System.out.println(schedule[2][1]);
                    break;
                case "wednesday":
                    System.out.println(schedule[3][1]);
                    break;
                case "thursday":
                    System.out.println(schedule[4][1]);
                    break;
                case "friday":
                    System.out.println(schedule[5][1]);
                    break;
                case "saturday":
                    System.out.println(schedule[6][1]);
                    break;
//                case "r":
//                    changeTask();
//                    break;
                case "exit":
                    isProgramWorking = false;
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    break;

            }
        }
    }
/*
    static private void changeTask() {
        String[] validDays = {"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"};
        Scanner myObj = new Scanner(System.in);

        String day = "";
        boolean isDayValid = true;

        while (isDayValid) {
            System.out.println("Please, input the day of the week to reschedule:");
            day = myObj.nextLine().toLowerCase().trim();

            for (String x : validDays) {
                if (x.equals(day)) {
                    isDayValid = false;
                }
            }
            if (isDayValid) {
                System.out.println("Sorry, I don't understand you, please try again.");
            } else {
                System.out.println("Please, input new tasks for " + day + ":");
            }
        }

        String task = myObj.nextLine();

        switch (day) {
            case "sunday":
                schedule[0][1] = task;
                break;
            case "monday":
                schedule[1][1] = task;
                break;
            case "tuesday":
                schedule[2][1] = task;
                break;
            case "wednesday":
                schedule[3][1] = task;
                break;
            case "thursday":
                schedule[4][1] = task;
                break;
            case "friday":
                schedule[5][1] = task;
                break;
            case "saturday":
                schedule[6][1] = task;
                break;
            default:
                System.out.println("Sorry, I don't understand you, please try again.");
                break;
        }
    }*/
}
