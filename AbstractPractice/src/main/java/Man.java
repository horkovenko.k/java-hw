public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, int birthday) {
        super(name, surname, birthday);
    }

    public Man(String name, String surname, int birthday, int iq, String[][] schedule, Pet pet) {
        super(name, surname, birthday, iq, schedule, pet);
    }

    public void repairCar() {
        System.out.println("Repairing Car Now!");
    }

    @Override
    public void greetPet() {
        System.out.printf("Hello Pet, %s! Here is your owner %s", pet.getNickname(), name);
        System.out.println();
    }
}
