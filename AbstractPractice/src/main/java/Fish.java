public class Fish extends Pet {
    Fish() {}

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age) {
        super(nickname, age);
    }

    Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("byl' byl' byl'");
    }
}
