public final class Woman extends Human {
    public Woman() {
    }

    public Woman(String name, String surname, int birthday) {
        super(name, surname, birthday);
    }

    Woman(String name, String surname, int birthday, int iq, String[][] schedule, Pet pet) {
        super(name, surname, birthday, iq, schedule, pet);
    }

    public void makeUp() {
        System.out.println("Making Up Now!");
    }

    @Override
    public void greetPet() {
        System.out.printf("Hello my dear, %s! I am %s", pet.getNickname(), name);
        System.out.println();
    }
}
