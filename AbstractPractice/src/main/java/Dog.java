public class Dog extends Pet implements Habits {
    Dog() {}

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age) {
        super(nickname, age);
    }

    Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("bark bark");
    }

    @Override
    public void foul() {
        System.out.println("Dirty dids had been done");
    }
}
