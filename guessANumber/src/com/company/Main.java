package com.company;

import java.util.Scanner;

public class Main {

    static String[][] quizQuestions = new String[3][3];

    static {
        quizQuestions[0][0] = "When did the World War II begin?";
        quizQuestions[0][1] = "When did the World War II end?";
        quizQuestions[0][2] = "When did the Second Opium War begin?";

        quizQuestions[1][0] = "1939";
        quizQuestions[1][1] = "1945";
        quizQuestions[1][2] = "1856";
    }

    public static void main(String[] args) {
        int numberWhichDefineQuestion = (int) (Math.random() * 3) + 1;

        Scanner userInput = new Scanner(System.in);
        System.out.println("Enter your nickname : ");

        String name = userInput.nextLine();

        System.out.println("Let the game begin: ");

        logQuestion(numberWhichDefineQuestion);

        int[] attempts = new int[10];
        int[] sortedAttempts;
        int i = 0;

        for (; ; ) {
            int userAnswer = userInput.nextInt();
            if (userAnswer == (int) userAnswer && i < 9) {
                attempts[i] = userAnswer;
                ++i;
            } else {
                System.out.println("Attempts out!");
                System.out.println("You lost!!!");

                sortedAttempts = bubbleSort(attempts);
                showArrayInLine(sortedAttempts);

                break;
            }

            if (
                    (userAnswer == 1856 && numberWhichDefineQuestion == 3) ||
                            (userAnswer == 1945 && numberWhichDefineQuestion == 2) ||
                            (userAnswer == 1939 && numberWhichDefineQuestion == 1)
            ) {
                System.out.println("Congratulations," + name + "!");
                System.out.println("You won!!!");

                sortedAttempts = bubbleSort(attempts);
                showArrayInLine(sortedAttempts);
                break;
            } else if (
                    (numberWhichDefineQuestion == 3 && (userAnswer > 1856)) ||
                            (numberWhichDefineQuestion == 2 && (userAnswer > 1945)) ||
                            (numberWhichDefineQuestion == 1 && (userAnswer > 1939))
            ) {
                logQuestion(numberWhichDefineQuestion);
                System.out.println("Your number is too big. Please, try again.");
            } else {
                logQuestion(numberWhichDefineQuestion);
                System.out.println("Your number is too small. Please, try again.");
            }
        }
    }

    static void logQuestion(int question) {
        switch (question) {
            case 1:
                System.out.println(quizQuestions[0][0]);
                break;
            case 2:
                System.out.println(quizQuestions[0][1]);
                break;
            case 3:
                System.out.println(quizQuestions[0][2]);
                break;
        }
    }

    static int[] bubbleSort(int[] sortedArr) {

        for (int i = 0; i < sortedArr.length; i++) {
            for (int j = -1; j < sortedArr.length - 1; j++) {
                int temp = sortedArr[j + 1];

                if (sortedArr[i] > temp) {
                    sortedArr[j + 1] = sortedArr[i];
                    sortedArr[i] = temp;
                }
            }
        }

        return sortedArr;
    }

    static void showArrayInLine(int[] array) {
        for (int item : array) {
            if (item != 0) {
                System.out.print(item + " ");
            }
        }
    }
}
