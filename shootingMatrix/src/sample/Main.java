package sample;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//   I decided to make this game with 3 attempts, 6 steps in which turn
        int[] attempts = new int[18];
        System.out.println("All set. Get ready to rumble!");

        int[] targetCoordinates = new int[6];

        for (int i = 0; i < targetCoordinates.length; i++) {
            int randomCoordinate = (int) (Math.random() * 5) + 1;
            targetCoordinates[i] = randomCoordinate;
        }

        System.out.println("Debug tip: ");

        for (int x : targetCoordinates) {
            System.out.print(x);
        }

        System.out.println();

        Scanner userInput = new Scanner(System.in);

        int userPassedValue = 1;
        int shootAttempt = 0;

        for (;;) {
            try {

                if (shootAttempt % 2 == 0) {
                    System.out.println("Enter a line: ");
                } else {
                    System.out.println("Enter a column: ");
                }

                userPassedValue = userInput.nextInt();

                if (userPassedValue < 1 || userPassedValue > 5) {
                    System.out.println("Please enter a number between 1 and 5!");
                    continue;
                } else {
//                  add appropriate coordinates into the array and increment counter
//                  later I'll pass attempts to drawMatrix function() to make rerender
                    attempts[shootAttempt] = userPassedValue;
                    shootAttempt++;
                }

//                1 turn is 6 coordinates from user which he passed through console
//                after 1 turn you can see the matrix
                if (shootAttempt % 6 == 0) {
                    System.out.println("Here is result!");
//                  next line render matrix and return boolean value which indicates is game over or not.
                    boolean isGameContinue = drawMatrix(attempts, targetCoordinates);

                    if (isGameContinue) {
                        System.out.println("You have won!!!");
                        break;
                    }

                }

                if (shootAttempt == 18) {
                    System.out.println("Game Over!");
                    break;
                }

            } catch (Exception e) {
                System.out.println("You pass a string! Please enter a number!!!");
                userInput.next();
            }
        }
    }

//  pass array for indicating "*" cells and targetCoordinates for "X" accordingly
    static boolean drawMatrix(int[] pass, int[] targetCoordinates) {
        boolean isUserWon = false;
        String[][] target = new String[6][12];
        int hitCounter = 0;
//      create Clean Matrix
        int topBorder = 0;
        for (int i = 0; i < target.length; i++) {
            for (int j = 0; j < target[0].length; j++) {
                if (j % 2 != 0) {
                    target[i][j] = "|";
                } else if (j != 0 && i != 0) {
                    target[i][j] = "-";
                }

                if (j % 2 == 0 && i == 0) {
                    if (topBorder == 8) break;
                    target[i][j] = Integer.toString(topBorder);
                    topBorder++;
                } else if (j % 2 != 0 && i == 0) {
                    target[i][j] = "|";
                }

                if (j == 0) {
                    target[i][j] = Integer.toString(i);
                }

            }
        }

//      render when you miss
        for (int i = 0; i < pass.length - 1; i++) {
            if (i % 2 != 0) continue; // two by two compare
            target[pass[i]][pass[i + 1] * 2] = "*";
        }

//      render when you hit
        for (int i = 0; i < pass.length - 1; i++) {
            for (int j = 0; j < targetCoordinates.length - 1; j++) {
                if (i % 2 != 0) continue; // two by two compare

                if (pass[i] == targetCoordinates[j] && pass[i + 1] == targetCoordinates[j + 1]) {
                    target[pass[i]][pass[i + 1] * 2] = "X";
                }

            }
        }

//      clearfix for 0 0 cell , because attempts array has a lot of 0 0 slots by default
//      then counting how many times you hit and render matrix
        target[0][0] = "0";
        for (int i = 0; i < target.length; i++) {
            for (int j = 0; j < target[0].length; j++) {
                System.out.print(target[i][j]);
                if (target[i][j] == "X") {
                    hitCounter++;
                }
            }
            System.out.println();
        }

//      Obvious end but there a little bug — additional X cells can appear... Which you cannot see in the debug tip.
//      Also, bug when generating two or more identical coordinates (example: (Debug tip: 555555)) not fixed too.
        if (hitCounter == 3) {
            isUserWon = true;
        }

        return isUserWon;
    }
}