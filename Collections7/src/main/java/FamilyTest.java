import org.junit.Assert;
import org.junit.Test;

public class FamilyTest {
    @Test
    public void removeChildTest() {
        Human h1 = new Human("n1", "s1", 1);
        Human h2 = new Human("n2", "s2", 1);
        Human h3 = new Human("n3", "s3", 1);
        Human h4 = new Human("n4", "s4", 1);
        Human h5 = new Human("n5", "s5", 1);
        Human[] kids = new Human[]{h3, h4, h5};
        Human[] kidsAddTest = new Human[]{h4, h5, h3};
        Human[] kidsRemoveTest = new Human[]{h4, h5};
        Fish p1 = new Fish();

        Family f = new Family(h1, h2, kids, p1);
        // Check that remove child working
        f.removeChild(0);
        Assert.assertArrayEquals(kidsRemoveTest, f.getChildren());

        // Check that remove child working when Index out of bounds
        f.removeChild(3);
        f.removeChild(-3);
        Assert.assertArrayEquals(kidsRemoveTest, f.getChildren());

        // Check that add child working
        f.addChild(h3);

        boolean isAddedChildCorrect = h3.equals(f.getChildren()[2]);

        Assert.assertArrayEquals(kidsAddTest, f.getChildren());
        Assert.assertEquals(h3.toString(), f.getChildren()[2].toString());
        Assert.assertSame(h3, f.getChildren()[2]);

        // Check that countFamily working
        Assert.assertTrue( f.countFamily() == 5 );
        f.removeChild(0);
        Assert.assertTrue( f.countFamily() == 4 );
        f.addChild(h5);
        Assert.assertTrue( f.countFamily() == 5 );

    }
}
