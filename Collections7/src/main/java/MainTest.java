import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void toStringTest() {
        Fish pet = new Fish();
        Human human = new Human();
        Human[] kids = new Human[]{human};
        Family family = new Family(human, human, kids, pet);

        boolean isPetContainSpecies = pet.toString().contains("Species");
        boolean isHumanContainScheduleAndBirthday= human.toString().contains("Birthday") && human.toString().contains("schedule");
        boolean isFamilyContainMotherAndFather= family.toString().contains("Mother") && family.toString().contains("Father");

        Assert.assertTrue("Pet toString method must contain Species", isPetContainSpecies);
        Assert.assertTrue("Human toString method must contain Birthday, schedule", isHumanContainScheduleAndBirthday);
        Assert.assertTrue("Family toString method must contain Mother, Father", isFamilyContainMotherAndFather);
    }
}
