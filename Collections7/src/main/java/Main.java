public class Main {

    public static void main(String[] args) {

        String[] habits = new String[]{"eat", "swim"};
        Fish pet = new Fish("gold", 1, 12, habits);
        pet.setSpecies(Species.Fish);
        DayOfWeek Monday = DayOfWeek.MONDAY;
        DayOfWeek Tuesday = DayOfWeek.TUESDAY;
        DayOfWeek Wednesday = DayOfWeek.WEDNESDAY;
        DayOfWeek Thursday = DayOfWeek.THURSDAY;
        DayOfWeek Friday = DayOfWeek.FRIDAY;
        DayOfWeek Saturday = DayOfWeek.SATURDAY;
        DayOfWeek Sunday = DayOfWeek.SUNDAY;

        String[][] schedule = new String[][]
                {
                        {Monday.name(), Tuesday.name(), Wednesday.name(), Thursday.name(), Friday.name(), Saturday.name(), Sunday.name()},
                        {"task1", "task2", "task3", "task4", "task5", "task6", "task7"}
                };
        Human human = new Human("John", "Rockefeller", 1839, 170, schedule, pet);

        Human human1 = new Human("Mary", "Rockefeller", 1830);
        Human human2 = new Human("John Jr.", "Rockefeller", 1869);
        Human human3 = new Human("Mary Jr.", "Rockefeller", 1879);
        Human human4 = new Human("Mary Jr. 1st", "Rockefeller", 1889);

        Human[] kids = new Human[]{human2, human3};
        Family family = new Family(human, human1, kids, pet);

        family.addChild(human4);
        family.removeChild(0);

        Human human5 = new Human("Danny", "Block", 1945);
        Human human6 = new Human("Mary", "Block", 1949);
        Human human7 = new Human("Bonita", "Block", 1970);

        Human[] kids1 = new Human[]{human7};
        Family family1 = new Family(human5, human6, kids1, pet);

        // 6 Homework
        System.out.println(pet);
        Woman w = new Woman("Bonita", "Cassia", 1948, 199, schedule, pet);
        w.greetPet();
        w.makeUp();

        habits = new String[]{"eat", "sleep", "meow"};
        DomesticCat pet1 = new DomesticCat("Myrzia", 1, 89, habits);
        Man m = new Man("Jack", "Jonex", 11, 999, schedule, pet1);
        pet1.setSpecies(Species.DomesticCat);
        System.out.println(pet1);

        m.greetPet();
        m.repairCar();

        habits = new String[]{"eat", "sleep", "bark"};
        Dog pet2 = new Dog("Spark", 1, 45, habits);
        pet2.setSpecies(Species.Dog);
        System.out.println(pet2);

        habits = new String[]{"recharge", "stand by"};
        RoboCat pet3 = new RoboCat("Elder", 1, 69, habits);
        pet3.setSpecies(Species.RoboCat);
        System.out.println(pet3);

        habits = new String[]{"stand still", "lie down"};
        RoboCat pet4 = new RoboCat("Toy", 1, 1, habits);
        pet4.setSpecies(Species.UNKNOWN);
        System.out.println(pet4);
    }

}
