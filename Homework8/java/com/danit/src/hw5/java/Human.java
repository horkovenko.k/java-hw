import java.util.Arrays;
import java.util.Objects;

enum DayOfWeek  {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}

public class Human {
    private String name;
    private String surname;
    private int birthday;
    private int iq;
    private String[][] schedule;

    public Human() {}

    public Human(String name, String surname, int birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public Human(String name, String surname, int birthday, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.iq = iq;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getBirthday() {
        return birthday;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return String.format("Name = %s, Surname = %s, Birthday = %d, iq = %d, schedule = %s",
                name, surname, birthday, iq, Arrays.deepToString(schedule) );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getBirthday() == human.getBirthday() &&
                getIq() == human.getIq() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname()) &&
                Arrays.equals(getSchedule(), human.getSchedule());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getName(), getSurname(), getBirthday(), getIq());
        result = 31 * result + Arrays.hashCode(getSchedule());
        return result;
    }

    @Override
    protected void finalize() {
        System.out.printf("This object removed. Name = %s, Surname = %s, Birthday = %d, iq = %d, schedule = %s",
                name, surname, birthday, iq, Arrays.deepToString(schedule) );
        System.out.println();
    }
}
