public class Main {

    public static void main(String[] args) {

        String[] habits = new String[]{"jump", "swim"};
        Pet pet = new Pet(Species.DOG, "doge", 1, 12, habits);
//        System.out.println(pet);

        DayOfWeek Monday = DayOfWeek.MONDAY;
        DayOfWeek Tuesday = DayOfWeek.TUESDAY;
        DayOfWeek Wednesday = DayOfWeek.WEDNESDAY;
        DayOfWeek Thursday = DayOfWeek.THURSDAY;
        DayOfWeek Friday = DayOfWeek.FRIDAY;
        DayOfWeek Saturday = DayOfWeek.SATURDAY;
        DayOfWeek Sunday = DayOfWeek.SUNDAY;

        String[][] schedule = new String[][]
                {
                        {Monday.name(), Tuesday.name(), Wednesday.name(), Thursday.name(), Friday.name(), Saturday.name(), Sunday.name()},
                        {"task1", "task2", "task3", "task4", "task5", "task6", "task7"}
                };
        Human human = new Human("John", "Rockefeller", 1839, 170, schedule);
//        System.out.println(human);

        Human human1 = new Human("Mary", "Rockefeller", 1830);
        Human human2 = new Human("John Jr.", "Rockefeller", 1869);
        Human human3 = new Human("Mary Jr.", "Rockefeller", 1879);
        Human human4 = new Human("Mary Jr. 1st", "Rockefeller", 1889);

        Human[] kids = new Human[]{human2, human3};
        Family family = new Family(human, human1, kids, pet);

        family.addChild(human4);
        family.removeChild(0);

//        System.out.println(family);

        Human human5 = new Human("Danny", "Block", 1945);
        Human human6 = new Human("Mary", "Block", 1949);
        Human human7 = new Human("Bonita", "Block", 1970);
        Pet pet1 = new Pet(Species.CAT, "Kote");

//        pet1.eat();
//        pet1.respond();
//        pet1.foul();

        Human[] kids1 = new Human[]{human7};
        Family family1 = new Family(human5, human6, kids1, pet1);

//        family1.describePet();
//        family1.greetPet();

//        System.out.println(family1);

        human1.finalize();
        pet.finalize();
        family.finalize();

        Human [] myHumans = new Human [100000];

        for (int i = 0; i < 100000; i++)
        {
            myHumans[i] = new Human();
        }


    }

}
