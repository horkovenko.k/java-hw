import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Family(Human mother, Human father, Human[] children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void greetPet() {
        System.out.format("Привет, %s", pet.getNickname());
    }

    public void describePet() {
        String trick = "почти не хитрый";
        if (pet.getTrickLevel() > 50) {
            trick = "очень хитрый";
        }

        System.out.format("У меня есть %s, ему %d лет, он %s", pet.getNickname(), pet.getAge(), trick);
    }

    public Human[] addChild(Human human) {
        Human[] res = new Human[children.length + 1];

        for (int i = 0; i < res.length; i++) {
            if (i == res.length - 1) {
                res[i] = human;
                break;
            }
            if (i < res.length - 1) {
                res[i] = children[i];
            }
        }

        children = res;

        return children;
    }

    public boolean removeChild(int index) {
        if (index > children.length || index < 0) return false;
        Human[] res = new Human[children.length - 1];

        for (int i = index; i < children.length - 1; i++) {
            children[i] = children[i + 1];
        }

        for (int i = 0; i < res.length; i++) {
            res[i] = children[i];
        }

        children = res;

        return true;
    }

    public int countFamily() {
        System.out.println(2 + children.length);
        return 2 + children.length;
    }

    @Override
    public String toString() {
        return String.format("Mother = { %s }, Father = { %s }, Children = { %s },  Pet = { %s }",
                mother.toString(), father.toString(), Arrays.deepToString(children), pet.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(getMother(), family.getMother()) &&
                Objects.equals(getFather(), family.getFather()) &&
                Arrays.equals(getChildren(), family.getChildren()) &&
                Objects.equals(getPet(), family.getPet());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getMother(), getFather(), getPet());
        result = 31 * result + Arrays.hashCode(getChildren());
        return result;
    }

    @Override
    protected void finalize() {
        System.out.printf("This object removed. Mother = { %s }, Father = { %s }, Children = { %s },  Pet = { %s }",
                mother.toString(), father.toString(), Arrays.deepToString(children), pet.toString() );
        System.out.println();
    }
}
