package com.danit.hw7.testing;

import com.danit.hw6.Enums.Species;
import com.danit.hw7.Enums.DayOfWeek;
import com.danit.hw7.HappyFamily.Family;
import com.danit.hw7.HappyFamily.Humans.Human;
import com.danit.hw7.HappyFamily.Pets.Fish;
import com.danit.hw7.HappyFamily.Pets.Pet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class MainTest {
    @Test
    @Before
    public void toStringTest() {
        Set<String> habits = new HashSet<String>();
        habits.add("swim");
        habits.add("eat");

        Fish pet = new Fish("f", 3, 20, habits);

        DayOfWeek Monday = DayOfWeek.MONDAY;

        Map<String, String> schedule = new HashMap<String, String>();

        schedule.put(Monday.name(), "task1");

        Human human = new Human("Name1", "Surname", 1800, 199, schedule);
        ArrayList<Human> kids = new ArrayList<Human>();
        kids.add(human);
        Set<Pet> pets = new HashSet<Pet>();
        pets.add(pet);
        Family family = new Family(human, human);

        family.setChildren(kids);
        family.setPet(pets);

        boolean isPetContainSpecies = pet.toString().contains("Species");
        boolean isHumanContainScheduleAndBirthday= human.toString().contains("Birthday") && human.toString().contains("schedule");
        boolean isFamilyContainMotherAndFather= family.toString().contains("Mother") && family.toString().contains("Father");

        Assert.assertTrue("Pet toString method must contain Species", isPetContainSpecies);
        Assert.assertTrue("Human toString method must contain Birthday, schedule", isHumanContainScheduleAndBirthday);
        Assert.assertTrue("Family toString method must contain Mother, Father", isFamilyContainMotherAndFather);
    }
}
