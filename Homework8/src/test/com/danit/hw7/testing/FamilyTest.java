package com.danit.hw7.testing;

import com.danit.hw7.HappyFamily.Family;
import com.danit.hw7.HappyFamily.Humans.Human;
import com.danit.hw7.HappyFamily.Pets.Fish;
import com.danit.hw7.HappyFamily.Pets.Pet;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class FamilyTest {
    Map<String, String> schedule = new HashMap<String, String>();
    private Human h1 = new Human("n1", "s1", 1, 1, schedule);
    private Human h3 = new Human("n3", "s3", 1, 1, schedule);
    private Human h2 = new Human("n2", "s2", 1, 1, schedule);
    private Human h4 = new Human("n4", "s4", 1, 1, schedule);
    private Human h5 = new Human("n5", "s5", 1, 1, schedule);
    private ArrayList<Human> kids = new ArrayList<Human>(); // kids = new Human[]{h3, h4, h5};
    private ArrayList<Human> kidsAddTest = new ArrayList<Human>(); // kidsAddTest = new Human[]{h3, h4, h5, h3};
    private ArrayList<Human> kidsRemoveTest = new ArrayList<Human>(); // kidsRemoveTest = new Human[]{h4, h5};
    private Fish p1 = new Fish();
    Set<Pet> pets = new HashSet<Pet>();


    {
        schedule.put("d1", "t1");
        pets.add(p1);

        kids.add(h3);
        kids.add(h4);
        kids.add(h5);

        kidsAddTest.add(h3);
        kidsAddTest.add(h4);
        kidsAddTest.add(h5);
        kidsAddTest.add(h3);

        kidsRemoveTest.add(h4);
        kidsRemoveTest.add(h5);
    }

    private Family f = new Family(h1, h2);

    {
        f.setChildren(kids);
        f.setPet(pets);
    }

    @Before
    public void init() {
        kids = new ArrayList<Human>();
        kids.add(h1);
        kids.add(h2);
    }

    @After
    public void teardown() {
        kids.clear();
    }

    @Test
    public void removeChildTestIndex() {
        f.removeChild(0);
        Assert.assertEquals(kidsRemoveTest, f.getChildren());
    }

    @Test
    public void removeChildTestObject() {
        f.removeChild(h3);
        Assert.assertEquals(kidsRemoveTest, f.getChildren());
    }

    @Test
    public void addChildTest() {
        f.addChild(h3);

        boolean isAddedChildCorrect = h3.equals(f.getChildren().get(3));

        Assert.assertEquals(kidsAddTest, f.getChildren());
        Assert.assertSame(h3, f.getChildren().get(3));
    }

    @Test
    public void countFamily() {
        Assert.assertEquals(5, f.countFamily());
        f.removeChild(0);
        Assert.assertEquals(4, f.countFamily());
        f.addChild(h5);
        Assert.assertEquals(5, f.countFamily());
    }

}
