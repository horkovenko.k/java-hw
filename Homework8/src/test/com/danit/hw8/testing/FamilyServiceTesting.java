package com.danit.hw8.testing;

import com.danit.hw8.DAOLayer.CollectionFamilyDao;
import com.danit.hw8.DAOLayer.FamilyService;
import com.danit.hw8.Enums.Species;
import com.danit.hw8.HappyFamily.Family;
import com.danit.hw8.HappyFamily.Humans.Human;
import com.danit.hw8.HappyFamily.Pets.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.*;

public class FamilyServiceTesting {


    private Set<String> habits = new HashSet<String>();

    {
        habits.add("swim");
        habits.add("eat");
    }

    private Fish pet = new Fish("gold", 1, 12, habits);

    private Dog pet1 = new Dog("DOGE", 1, 12, habits);

    private DomesticCat pet2 = new DomesticCat("KOTE", 1, 12, habits);

    private RoboCat pet3 = new RoboCat("ROBOT", 1, 12, habits);

    private Map<String, String> schedule = new HashMap<String, String>();

    {
        schedule.put("Monday", "task1");
    }

    private Human hf1 = new Human("mama1", "Surname", "11/11/1991", 199, schedule);
    private Human hm1 = new Human("papa1", "Surname", "11/11/1991", 199, schedule);
    private Human kid1 = new Human("kid1", "Surname", "11/11/1991", 199, schedule);

    private Human hf2 = new Human("mama2", "Surname", "11/11/1991", 199, schedule);
    private Human hm2 = new Human("papa2", "Surname", "11/11/1991", 199, schedule);
    private Human kid2 = new Human("kid2", "Surname", "11/11/1991", 199, schedule);

    private Human hf3 = new Human("mama3", "Surname", "11/11/1991", 199, schedule);
    private Human hm3 = new Human("papa3", "Surname", "11/11/1991", 199, schedule);
    private Human kid3 = new Human("kid3", "Surname", "11/11/1991", 199, schedule);

    private Human hf4 = new Human("mama4", "Surname", "11/11/1991", 199, schedule);
    private Human hm4 = new Human("papa4", "Surname", "11/11/1991", 199, schedule);
    private Human kid4 = new Human("kid4", "Surname", "11/11/1991", 199, schedule);

    private List<Human> kids1 = new ArrayList<>();

    {
        kids1.add(kid1);
    }

    private Set<Pet> pets1 = new HashSet<>();

    {
        pets1.add(pet);
    }

    private List<Human> kids2 = new ArrayList<>();

    {
        kids2.add(kid2);
    }

    private Set<Pet> pets2 = new HashSet<>();

    {
        pets2.add(pet1);
    }

    private List<Human> kids3 = new ArrayList<>();

    {
        kids3.add(kid3);
    }

    private Set<Pet> pets3 = new HashSet<>();

    {
        pets3.add(pet2);
    }

    private List<Human> kids4 = new ArrayList<>();

    {
        kids4.add(kid4);
    }

    private Set<Pet> pets4 = new HashSet<>();

    {
        pets4.add(pet3);
    }

    private Family f1 = new Family(hm1, hf1);

    {
        f1.setChildren(kids1);
        f1.setPet(pets1);
    }

    private Family f2 = new Family(hm2, hf2);

    {
        f2.setChildren(kids2);
        f2.setPet(pets2);
    }

    private Family f3 = new Family(hm3, hf3);

    {
        f3.setChildren(kids3);
        f3.setPet(pets3);
    }

    private Family f4 = new Family(hm4, hf4);

    {
        f4.setChildren(kids4);
        f4.setPet(pets4);
    }

    private List<Family> fs = new ArrayList<>();

    {
        fs.add(f1);
        fs.add(f2);
        fs.add(f3);
    }

    private CollectionFamilyDao db = new CollectionFamilyDao();

    {
        db.setFamilies(fs);
    }

    private FamilyService fserv = new FamilyService();

    {
        fserv.setFamilyDao(db);
    }

    public FamilyServiceTesting() throws ParseException {
    }

    @Before
    public void isTestingDataCreated() {
        FamilyService fserv = new FamilyService();
    }

    @After
    public void isTestingDataRemoved() {
        fserv.getFamilyDao().getFamilies().clear();
    }

    @Test
    public void getFamilyDaoReturnRightArray() {
        Assert.assertEquals(fs, fserv.getFamilyDao().getFamilies());
    }

    @Test
    public void getAllFamiliesReturnRightArray() {
        Assert.assertEquals(db, fserv.getFamilyDao());
    }

    @Test
    public void displayAllFamiliesReturnRightArray() {
        Assert.assertEquals(fs, fserv.displayAllFamilies());
    }

    @Test
    public void getFamiliesBiggerThanReturnRightArray() {
        Assert.assertEquals(3, fserv.getFamiliesBiggerThan(2).size());
        Assert.assertEquals(0, fserv.getFamiliesBiggerThan(4).size());
    }

    @Test
    public void getFamiliesLessThanReturnRightArray() {
        Assert.assertEquals(0, fserv.getFamiliesLessThan(2).size());
        Assert.assertEquals(3, fserv.getFamiliesLessThan(4).size());
    }

    @Test
    public void countFamiliesWithMemberNumberReturnRightArray() {
        Assert.assertEquals(3, fserv.countFamiliesWithMemberNumber(3));
        Assert.assertEquals(0, fserv.countFamiliesWithMemberNumber(2));
    }

    @Test
    public void createNewFamilyReturnRightArray() {
        fs.add(new Family(hm4, hf4));

        Assert.assertEquals(fs, fserv.createNewFamily(hm4, hf4));
    }

    @Test
    public void deleteFamilyByIndexReturnRightArray() {
        fs.remove(1);
        fserv.deleteFamilyByIndex(1);
        Assert.assertEquals(fs, fserv.getFamilyDao().getFamilies());
    }

    @Test
    public void bornChildReturnRightArray() throws ParseException {
        fserv.bornChild(f1, "pop", "mom");
        Assert.assertEquals(2, fserv.getFamilyById(0).getChildren().size());
        Assert.assertEquals("pop", fserv.getFamilyById(0).getChildren().get(1).getName());
    }

    @Test
    public void adoptChildReturnRightArray() {
        fserv.adoptChild(f1, kid1);
        Assert.assertEquals(2, fserv.getFamilyById(0).getChildren().size());
    }

    @Test
    public void deleteAllChildrenOlderThenReturnRightArray() {
        fserv.deleteAllChildrenOlderThen(10);
        Assert.assertEquals(0, fserv.getFamilyById(1).getChildren().size());
        Assert.assertEquals(0, fserv.getFamilyById(0).getChildren().size());
        Assert.assertEquals(0, fserv.getFamilyById(2).getChildren().size());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(3, fserv.count());
    }

    @Test
    public void getFamilyByIdTest() {
        Assert.assertEquals(f1, fserv.getFamilyById(0));
        Assert.assertEquals(f3, fserv.getFamilyById(2));
    }

    @Test
    public void getPetsTest() {
        Assert.assertEquals(pets1, fserv.getPets(0));
        Assert.assertEquals(pets3, fserv.getPets(2));
    }

    @Test
    public void addPetTest() {
        pets1.add(pet3);
        fserv.addPet(pet3, 0);

        Assert.assertEquals(pets1, fserv.getPets(0));
    }

}
