package com.danit.hw8.ConsoleApp;

import com.danit.hw8.DAOLayer.CollectionFamilyDao;
import com.danit.hw8.DAOLayer.FamilyOverflowException;
import com.danit.hw8.Enums.DayOfWeek;
import com.danit.hw8.HappyFamily.Humans.Human;
import com.danit.hw8.HappyFamily.Humans.Man;
import com.danit.hw8.DAOLayer.FamilyController;
import com.danit.hw8.HappyFamily.Family;
import com.danit.hw8.HappyFamily.Humans.Woman;
import com.danit.hw8.HappyFamily.Pets.Dog;
import com.danit.hw8.HappyFamily.Pets.Fish;
import com.danit.hw8.HappyFamily.Pets.Pet;
import com.danit.hw8.HappyFamily.Pets.RoboCat;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;

public class App {
    private FamilyController familyControllerToDisplay;
    private String mainMenu =
            "- 0. Завершить работу \n" +
            "- 1. Заполнить тестовыми данными \n" +
            "- 2. Отобразить весь список семей \n" +
            "- 3. Отобразить список семей, где количество людей больше заданного \n" +
            "- 4. Отобразить список семей, где количество людей меньше заданного \n" +
            "- 5. Подсчитать количество семей, где количество членов равно \n" +
            "- 6. Создать новую семью \n" +
            "- 7. Удалить семью по индексу семьи в общем списке \n" +
            "- 8. Редактировать семью по индексу семьи в общем списке \n" +
            "- 9. Удалить всех детей старше возраста \n" +
            "- 10. Сохранить семью в файл \n" +
            "- 11. Загрузить данные из файла \n";

    private String subMenuForEditFamily =
            "- 1. Родить ребенка \n" +
            "- 2. Усыновить ребенка \n" +
            "- 3. Вернуться в главное меню \n";

    public void startApp() throws ParseException {
        System.out.print(mainMenu);
        processUserInput();
    }

    private void processUserInput() throws ParseException {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Choose menu item: ");
        Map<Integer, Runnable> map = new HashMap<Integer, Runnable>();
        map.put(1, new Runnable() {
            public void run() {
                try {
                    fillTableWithTestData();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        map.put(2, this::displayAllFamiliesWithIndexes);

        map.put(3, this::displayAllFamiliesBiggerThan);

        map.put(4, this::displayAllFamiliesLessThan);

        map.put(5, this::countAllFamiliesWhereNumberOfMembersEqualTo);

        map.put(6, new Runnable() {
            public void run() {
                try {
                    createNewFamilyAndAddItToList();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        map.put(7, this::deleteFamilyFromList);

        map.put(8, new Runnable() {
            public void run() {
                try {
                    editFamilyFromListByIndex();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        map.put(9, this::deleteAllChildrenOlderThanInAllFamilies);

        map.put(10, new Runnable() {
            public void run() {

                try {
                    saveFamiliesToAFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        map.put(11, new Runnable() {
            public void run() {

                try {
                    getDataFromAFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        map.put(0, () -> showContextMenu("Exit"));

        while (true) {
            String menuItem = userInput.nextLine();

            if (isNumeric(menuItem) &&
                Integer.parseInt(menuItem) > 0 &&
                Integer.parseInt(menuItem) <= 11
            ) {
                map.get(Integer.parseInt(menuItem)).run();
            } else if (isNumeric(menuItem) && Integer.parseInt(menuItem) == 0) {
                break;
            } else {
                System.out.println("Wrong data! Enter number between 1 and 11. Or 0 to exit!");
            }
        }
    }

    private void fillTableWithTestData() throws ParseException {

        DayOfWeek Monday = DayOfWeek.MONDAY;
        DayOfWeek Tuesday = DayOfWeek.TUESDAY;

        Map<String, String> schedule = new HashMap<String, String>();

        schedule.put(Monday.name(), "task1");
        schedule.put(Tuesday.name(), "task2");

        //Family1

        Set<String> fishHabits = new HashSet<String>();
        fishHabits.add("swim");
        fishHabits.add("eat");
        Fish fish1 = new Fish("King", 5, 40, fishHabits);

        Set<String> catHabits = new HashSet<String>();
        catHabits.add("jump");
        catHabits.add("meow");
        Fish cat1 = new Fish("Frank", 5, 80, catHabits);

        Set<Pet> petsForFamily1 = new HashSet<>();
        petsForFamily1.add(fish1);
        petsForFamily1.add(cat1);

        Man father1 = new Man("Frank", "Sinatra", "11/11/1975", 199, schedule);
        Woman mother1 = new Woman("Nikki", "Sinatra", "11/11/1965", 199, schedule);

        Man kid11 = new Man("John", "Sinatra", "11/11/1995", 199, schedule);
        Woman kid12 = new Woman("Mary", "Sinatra", "11/11/1992", 199, schedule);

        List<Human> kidsList1 = new ArrayList<>();
        kidsList1.add(kid11);
        kidsList1.add(kid12);

        Family f1 = new Family(mother1, father1);
        f1.setPet(petsForFamily1);
        f1.setChildren(kidsList1);

        //Family2

        Set<String> dogHabits = new HashSet<String>();
        dogHabits.add("run");
        dogHabits.add("eat");
        Dog dog2 = new Dog("Jakie", 5, 60, dogHabits);

        Set<String> catHabits2 = new HashSet<String>();
        catHabits2.add("catch fish");
        catHabits2.add("meow");
        Fish cat2 = new Fish("Lori", 2, 80, catHabits);

        Set<Pet> petsForFamily2 = new HashSet<>();
        petsForFamily2.add(dog2);
        petsForFamily2.add(cat2);

        Man father2 = new Man("Jerome", "Jackson", "11/11/1976", 199, schedule);
        Woman mother2 = new Woman("Liza", "Jackson", "11/11/1972", 199, schedule);

        Man kid21 = new Man("Gilbert", "Jackson", "11/11/1991", 199, schedule);
        Woman kid22 = new Woman("Lucy", "Jackson", "11/11/1992", 199, schedule);
        Woman kid23 = new Woman("Bonita", "Jackson", "11/11/1993", 199, schedule);

        List<Human> kidsList2 = new ArrayList<>();
        kidsList2.add(kid21);
        kidsList2.add(kid22);
        kidsList2.add(kid23);

        Family f2 = new Family(mother2, father2);
        f2.setPet(petsForFamily2);
        f2.setChildren(kidsList2);

        //Family3

        Set<String> robotHabits = new HashSet<String>();
        robotHabits.add("walk");
        robotHabits.add("recharge");
        RoboCat robot3 = new RoboCat("king", 5, 90, robotHabits);

        Set<String> dogHabits3 = new HashSet<String>();
        dogHabits3.add("jump");
        dogHabits3.add("meow");
        Dog dog3 = new Dog("Frank", 5, 80, dogHabits3);

        Set<Pet> petsForFamily3 = new HashSet<>();
        petsForFamily3.add(robot3);
        petsForFamily3.add(dog3);

        Man father3 = new Man("Mike", "Piterson", "11/11/1970", 199, schedule);
        Woman mother3 = new Woman("Angela", "Piterson", "11/11/1965", 199, schedule);

        Man kid31 = new Man("Piter", "Piterson", "11/11/1995", 199, schedule);
        Man kid32 = new Man("Jack", "Piterson", "11/11/1996", 199, schedule);
        Woman kid33 = new Woman("Monika", "Piterson", "11/11/1992", 199, schedule);

        List<Human> kidsList3 = new ArrayList<>();
        kidsList3.add(kid31);
        kidsList3.add(kid32);
        kidsList3.add(kid33);

        Family f3 = new Family(mother3, father3);
        f3.setPet(petsForFamily3);
        f3.setChildren(kidsList3);


        List<Family> fs = new ArrayList<>();
        fs.add(f1);
        fs.add(f2);
        fs.add(f3);

        //DB Connection

        CollectionFamilyDao db = new CollectionFamilyDao();
        db.setFamilies(fs);

        FamilyController familyController = new FamilyController();
        familyController.setFamilyDao(db);

        this.familyControllerToDisplay = familyController;

        showContextMenu("Database connected successfully!");
    }

    private void displayAllFamiliesWithIndexes() {
        if (isDBEmpty()) {
            List<Family> result = familyControllerToDisplay.getFamilyDao().getFamilies();
            String displayInfo = "";

            for (int i = 0; i < result.size(); i++) {
                int familyIndex = i + 1;
                displayInfo += "Index Of Family " + familyIndex + "\n" + result.get(i).prettyFormat() + "\n"
                + "////////////////////////////////////////////////////////////////////////////////////////\n";
            }

            System.out.println(displayInfo);

            showContextMenu("All families are displayed with Indexes!");
        }
    }

    private void displayAllFamiliesBiggerThan() {
        if (isDBEmpty()) {
            int numberOfMembersInFamily = processingInputOfFamilyMembersNumber();

            List<Family> result = familyControllerToDisplay.getFamilyDao().getFamilies();
            String displayInfo = "";

            for (int i = 0; i < result.size(); i++) {
                int familyIndex = i + 1;
                if (result.get(i).countFamily() > numberOfMembersInFamily)
                displayInfo += "Index Of Family " + familyIndex + "\n" + result.get(i).prettyFormat() + "\n"
                        + "////////////////////////////////////////////////////////////////////////////////////////\n";
            }

            System.out.println(displayInfo.equals("") ? "There are no such families!" : displayInfo);

            showContextMenu("All families that greater than " + numberOfMembersInFamily + " are displayed!");
        }
    }

    private void displayAllFamiliesLessThan() {
        if (isDBEmpty()) {
            int numberOfMembersInFamily = processingInputOfFamilyMembersNumber();

            List<Family> result = familyControllerToDisplay.getFamilyDao().getFamilies();
            String displayInfo = "";

            for (int i = 0; i < result.size(); i++) {
                int familyIndex = i + 1;
                if (result.get(i).countFamily() < numberOfMembersInFamily)
                    displayInfo += "Index Of Family " + familyIndex + "\n" + result.get(i).prettyFormat() + "\n"
                            + "////////////////////////////////////////////////////////////////////////////////////////\n";
            }

            System.out.println(displayInfo.equals("") ? "There are no such families!" : displayInfo);

            showContextMenu("All families that less than " + numberOfMembersInFamily + " are displayed!");
        }
    }

    private void countAllFamiliesWhereNumberOfMembersEqualTo() {
        if (isDBEmpty()) {
            int numberOfMembersInFamily = processingInputOfFamilyMembersNumber();

            List<Family> result = familyControllerToDisplay.getFamilyDao().getFamilies();

            int numberOfFamilies = 0;

            for (Family family : result) {
                if (family.countFamily() == numberOfMembersInFamily) {
                    numberOfFamilies++;
                }
            }

            showContextMenu(numberOfFamilies + " Family where number of members are equal to " + numberOfMembersInFamily);
        }
    }

    private void createNewFamilyAndAddItToList() throws ParseException {
        Human[] humans = new Human[2];
        String[] humanData;

        for (int i = 0; i < humans.length; i++) {
            String info = i == 0 ? "Enter mother data:\n" : "Enter father data:\n";
            System.out.println(info);

            humanData = newHumanData();

            int day = Integer.parseInt(humanData[2]);
            int month = Integer.parseInt(humanData[3]);
            int year = Integer.parseInt(humanData[4]);
            int iq = Integer.parseInt(humanData[5]);

            humans[i] = new Human(humanData[0], humanData[1], String.format("%d/%d/%d", day, month, year), iq);

        }

        if (familyControllerToDisplay != null) {
            familyControllerToDisplay.createNewFamily(humans[0], humans[1]);
        } else {
            List<Family> fs = new ArrayList<>();
            Family family = new Family(humans[0], humans[1]);
            fs.add(family);

            //DB Connection

            CollectionFamilyDao db = new CollectionFamilyDao();
            db.setFamilies(fs);

            FamilyController familyController = new FamilyController();
            familyController.setFamilyDao(db);

            familyControllerToDisplay = familyController;

        }
        showContextMenu("Added to database successfully!");
    }

    private void deleteFamilyFromList() {
        if (isDBEmpty()) {
            System.out.println("Enter index to delete family. Number of items " + familyControllerToDisplay.count());

            int indexToDelete = processingInputOfFamilyIndex();

            familyControllerToDisplay.deleteFamilyByIndex(indexToDelete);

            showContextMenu("Item removed.");
        }
    }

    private void editFamilyFromListByIndex() throws ParseException {
        if (isDBEmpty()) {
            System.out.println(subMenuForEditFamily);

            Scanner userInput = new Scanner(System.in);
            boolean subMenuFlag = true;

            while (subMenuFlag) {
                String item = userInput.nextLine();
                switch (item) {
                    case "1":
                        try {
                            bornChild();
                        } catch (FamilyOverflowException err) {
                            System.out.println(err.getMessage());
                            System.out.println(subMenuForEditFamily);
                        }
                        break;
                    case "2":
                        try {
                            adoptChild();
                        } catch (FamilyOverflowException err) {
                            System.out.println(err.getMessage());
                            System.out.println(subMenuForEditFamily);
                        }
                        break;
                    case "3":
                        subMenuFlag = false;
                        System.out.println(mainMenu);
                        break;
                    default:
                        System.out.println("Wrong input only 1, 2, 3 allowed!");
                }
            }

        }
    }

    private void bornChild() throws ParseException {
        System.out.println("Born Kid Data:");

        extractDataFromHuman(true);
        System.out.println("Child born!");

    }

    private void adoptChild() throws ParseException {
        System.out.println("Adopted Kid Data:");

        extractDataFromHuman(false);
        System.out.println("Child adopted!");
    }

    private void deleteAllChildrenOlderThanInAllFamilies() {
        if (isDBEmpty()) {
            Scanner userInput = new Scanner(System.in);

            int ageOfChildren = processingInputOfHumanAge();

            familyControllerToDisplay.deleteAllChildrenOlderThen(ageOfChildren);
            showContextMenu("All kids older than " + ageOfChildren + " year deleted!");
        }
    }

    private void saveFamiliesToAFile() throws IOException {
        if (isDBEmpty()) {
            System.out.println(this.familyControllerToDisplay.getAllFamilies());
            this.familyControllerToDisplay.getFamilyDao().loadData(this.familyControllerToDisplay.getAllFamilies());
            showContextMenu("Data saved!");
        }
    }

    private void getDataFromAFile() throws IOException {
        File file = new File("src\\main\\java\\com\\danit\\hw8\\data.txt");

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.setVisibility(VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        Family[] family = objectMapper.readValue(file, Family[].class);

        CollectionFamilyDao db = new CollectionFamilyDao();

        List<Family> familiesToAdd = new ArrayList<>();
        Collections.addAll(familiesToAdd, family);

        db.setFamilies(familiesToAdd);

        FamilyController familyController = new FamilyController();
        familyController.setFamilyDao(db);

        this.familyControllerToDisplay = familyController;

        showContextMenu("Database connected successfully!");

        showContextMenu("Data fetched!");

    }

    // Methods for prettier format
    private int processingInputOfFamilyMembersNumber() {
        Scanner userInput = new Scanner(System.in);
        int membersNumber = 0;
        System.out.println("Enter Number Of Members: ");

        while (true) {
            String numberOfFamilyMembers = userInput.nextLine();

            if (isNumeric(numberOfFamilyMembers)) {
                if (Integer.parseInt(numberOfFamilyMembers) >= 2) {
                    membersNumber = Integer.parseInt(numberOfFamilyMembers);
                    break;
                } else {
                    System.out.println("Enter number bigger than 2");
                }
            } else {
                System.out.println("Enter integer!");
            }
        }

        return membersNumber;
    }

    private int processingInputOfFamilyIndex() {
        int boundary = familyControllerToDisplay.count();
        Scanner userInput = new Scanner(System.in);
        int index = 0;
        int border = boundary - 1;
        System.out.println("Enter Index: ");

        while (true) {
            String familyIndex = userInput.nextLine();

            if (isNumeric(familyIndex) &&
                    Integer.parseInt(familyIndex) >= 0 &&
                    Integer.parseInt(familyIndex) < boundary
            ) {
                index = Integer.parseInt(familyIndex);
                break;
            } else {
                System.out.println("Enter a Number within 0 and " + border );
            }
        }

        return index;
    }

    private int processingInputOfHumanAge() {
        Scanner userInput = new Scanner(System.in);
        int age = 0;
        System.out.println("Enter Age: ");

        while (true) {
            String numberOfFamilyMembers = userInput.nextLine();

            if (isNumeric(numberOfFamilyMembers) && Integer.parseInt(numberOfFamilyMembers) > 0) {
                    age = Integer.parseInt(numberOfFamilyMembers);
                    break;
            } else {
                System.out.println("Enter integer greater than 0!");
            }
        }

        return age;
    }

    private String[] newHumanData() {
        Scanner userInput = new Scanner(System.in);

        System.out.println("Enter name:");
        String name = userInput.nextLine();
        System.out.println("Enter surname:");
        String surname = userInput.nextLine();

        System.out.println("Enter birth year yyyy:");
        String birthYear = "";
        while (true) {
            String birthYYYY = userInput.nextLine();

            if (isNumeric(birthYYYY) && birthYYYY.length() == 4) {
                birthYear = birthYYYY;
                break;
            } else {
                System.out.println("Wrong input");
            }
        }

        System.out.println("Enter birth month mm:");
        String birthMonth = "";
        while (true) {
            String birthMM = userInput.nextLine();

            if (isNumeric(birthMM) && birthMM.length() == 2
                    && (Integer.parseInt(birthMM) >= 1 && Integer.parseInt(birthMM) <= 12)) {
                birthMonth = birthMM;
                break;
            } else {
                System.out.println("Wrong input");
            }
        }

        System.out.println("Enter birth day dd:");
        String birthDay = "";
        while (true) {
            String birthDD = userInput.nextLine();

            if (isNumeric(birthDD) && birthDD.length() == 2
                    && (Integer.parseInt(birthDD) >= 1 && Integer.parseInt(birthDD) <= 31)) {
                birthDay = birthDD;
                break;
            } else {
                System.out.println("Wrong input");
            }
        }

        System.out.println("Enter iq level:");
        String iq = "";
        while (true) {
            String iqLevel = userInput.nextLine();

            if (isNumeric(iqLevel)) {
                iq = iqLevel;
                break;
            } else {
                System.out.println("Wrong input");
            }
        }

        return new String[]{name, surname, birthDay, birthMonth, birthYear, iq};
    }

    private boolean isDBEmpty() {
        if (familyControllerToDisplay == null) {
            System.out.println("Database is empty!");
            return false;
        }

        return true;
    }

    private void confirmChangesToDB(int index, Human kid) {
        if (familyControllerToDisplay.getAllFamilies().get(index).countFamily() > 5) {
            throw new FamilyOverflowException("Maximum Reached");
        }

        List<Human> children = new ArrayList<>();
        if (familyControllerToDisplay.getAllFamilies().get(index).getChildren() != null)
            children.addAll(familyControllerToDisplay.getAllFamilies().get(index).getChildren());
        children.add(kid);

        familyControllerToDisplay.getAllFamilies().get(index).setChildren(children);
    }

    private void extractDataFromHuman(boolean isChildNew) throws ParseException {
        String[] humanData = newHumanData();

        int day = Integer.parseInt(humanData[2]);
        int month = Integer.parseInt(humanData[3]);
        int year = Integer.parseInt(humanData[4]);
        int iq = Integer.parseInt(humanData[5]);


        System.out.println("Family index, please. Number of families " + familyControllerToDisplay.getAllFamilies().size());
        int index = processingInputOfFamilyIndex();

        if (isChildNew) {
            LocalDate currentDate = LocalDate.now();
            day = currentDate.getDayOfMonth();
            month = currentDate.getMonthValue();
            year = currentDate.getYear();
        }

        Human kid = new Human(humanData[0], humanData[1], String.format("%d/%d/%d", day, month, year), iq);


        confirmChangesToDB(index, kid);


        System.out.println(subMenuForEditFamily);
    }

    private static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }

        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }

        return true;
    }

    private void showContextMenu(String msg) {
        System.out.println(mainMenu);
        System.out.println(msg);
        System.out.println("Choose menu item: ");
    }

}
