package com.danit.hw8.Interfaces;

import com.danit.hw8.HappyFamily.Family;
import com.fasterxml.jackson.databind.JsonMappingException;

import java.io.IOException;
import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    List<Family> saveFamily(Family family);

    void loadData(List<Family> families) throws IOException, JsonMappingException, Throwable;

    String getData();
}
