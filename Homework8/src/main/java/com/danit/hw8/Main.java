package com.danit.hw8;

import com.danit.hw8.ConsoleApp.App;
import com.danit.hw8.HappyFamily.Humans.Man;
import com.danit.hw8.DAOLayer.CollectionFamilyDao;
import com.danit.hw8.DAOLayer.FamilyController;
import com.danit.hw8.DAOLayer.FamilyService;
import com.danit.hw8.Enums.DayOfWeek;
import com.danit.hw8.Enums.Species;
import com.danit.hw8.HappyFamily.Family;
import com.danit.hw8.HappyFamily.Humans.Human;
import com.danit.hw8.HappyFamily.Humans.Woman;
import com.danit.hw8.HappyFamily.Pets.Dog;
import com.danit.hw8.HappyFamily.Pets.DomesticCat;
import com.danit.hw8.HappyFamily.Pets.Fish;
import com.danit.hw8.HappyFamily.Pets.Pet;
import org.w3c.dom.ls.LSOutput;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws ParseException {

        Set<String> habits = new HashSet<String>();
        habits.add("swim");
        habits.add("eat");
        Fish pet = new Fish("gold", 1, 12, habits);
        DomesticCat pet1 = new DomesticCat("kote", 2, 99, habits);
        Dog pet2 = new Dog("doge", 3, 69, habits);
        Dog pet3 = new Dog("haski", 3, 69, habits);


        DayOfWeek Monday = DayOfWeek.MONDAY;
        DayOfWeek Tuesday = DayOfWeek.TUESDAY;

        Map<String, String> schedule = new HashMap<String, String>();

        schedule.put(Monday.name(), "task1");
        schedule.put(Tuesday.name(), "task2");

        Human h = new Human("Family0", "Surname", "18/11/1956", 199, schedule);

        Human h1 = new Human("Name1", "Surname", "18/11/1956", 199, schedule);
        Human h2 = new Human("Name2", "Surname", "18/11/1956", 199, schedule);
        Human h3 = new Human("Family1", "Surname", "18/11/1956", 199, schedule);
        Human h4 = new Human("Family2", "Surname", "18/11/1956", 199, schedule);

        Human h5 = new Human("adoptedChild", "Surname", "18/11/2016", 199, schedule);
        Human kid1 = new Human("kid1", "Surname", "18/11/2015", 199, schedule);
        Human kid2 = new Woman("kid2", "Surname", "18/11/1995", 199, schedule);
        Human kid3 = new Man("kid3", "Surname", "18/11/1991", 199, schedule);
        Human kid4 = new Woman("KID_4", "TEST", "18/11/2000", 199, schedule);

        List<Human> kids1 = new ArrayList<>();
        kids1.add(kid1);
        List<Human> kids2 = new ArrayList<>();
        kids2.add(kid2);
        kids2.add(kid1);
        kids2.add(kid3);
        List<Human> kids3 = new ArrayList<>();
        kids3.add(kid3);

        Set<Pet> pets = new HashSet<>();
        pets.add(pet);
        pets.add(pet1);
        Set<Pet> pets1 = new HashSet<>();
        pets1.add(pet1);
        pets1.add(pet);
        pets1.add(pet2);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(pet2);

        Family f = new Family(h, h1);
        Family f1 = new Family(h3, h1);
        Family f2 = new Family(h4, h1);
        Family f3 = new Family(h5, h1);

        f.setChildren(kids1);
        f.setPet(pets);
        f1.setChildren(kids2);
        f1.setPet(pets1);
        f2.setChildren(kids3);
        f2.setPet(pets2);
        f3.setChildren(kids3);
        f3.setPet(pets);

        List<Family> fs = new ArrayList<>();
        fs.add(f);
        fs.add(f1);
        fs.add(f2);

        CollectionFamilyDao db = new CollectionFamilyDao();
        db.setFamilies(fs);

        List<Human> kids4 = new ArrayList<>();
        kids4.add(h);
        kids4.add(h);
        kids4.add(h);

        FamilyService fserv = new FamilyService();
        fserv.setFamilyDao(db);

        FamilyController fcon = new FamilyController();

        fcon.setFamilyDao(db);

//        System.out.println(fcon.getAllFamilies());

//        fcon.getFamiliesBiggerThan(4);

//        fcon.getFamiliesLessThan(4);

//        System.out.println(fcon.countFamiliesWithMemberNumber(2));

//        fcon.displayAllFamilies();

//        fcon.createNewFamily(h1, h2);

//        fcon.deleteFamilyByIndex(2);

//        fcon.bornChild(f1, "mather", "father");

//        fcon.adoptChild(f1, h1);


//        System.out.println(fcon.count());

//        System.out.println(fcon.getFamilyById(1));

//        System.out.println(fcon.getPets(1));

//        fcon.addPet(pet3, 1);

//        List<Family> test = new ArrayList<>();

//        test = fcon.getFamiliesBiggerThan(2);
//        test = fcon.getFamiliesLessThan(10);

//        System.out.println(test);

//        fcon.deleteAllChildrenOlderThen(20);
//
//        for (Family l : fcon.displayAllFamilies()) {
//            System.out.print(l.prettyFormat());
//        }
//
//       String s = h1.describeAge();
//        System.out.println(s);
//        System.out.println(h1);
//
//        System.out.println( kid1.getBirthdayYear());
//        Human childy = new Human("n1", "s1", "11/12/2012", 100);
//        System.out.println(childy);

//        f.addChild(h2);
//        f.addChild(h4);
//
//        f.prettyFormat();

//        Human humanx = new Human("nqme", "sur", "11/12/1991");
//        long l = humanx.getUnixMillisTimestamp();
//        System.out.println(l);


        App consoleApp = new App();

        consoleApp.startApp();

//        System.out.println(f1.toJson());

    }
}
