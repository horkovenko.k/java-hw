package com.danit.hw8.HappyFamily.Humans;

import java.text.ParseException;
import java.util.Map;

public final class Woman extends Human {
    public Woman() {
    }

    public Woman(String name, String surname, String birthDate) throws ParseException {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, String birthDate, int iq) throws ParseException {
        super(name, surname, birthDate, iq);
    }

    public Woman(String name, String surname, String birthDate, int iq, Map<String, String> schedule) throws ParseException {
        super(name, surname, birthDate, iq, schedule);
    }

    public void makeUp() {
        System.out.println("Making Up Now!");
    }

    @Override
    public void greetPet() {
        System.out.printf("Hello my dear, %s! I am %s", family.getPet(), name);
        System.out.println();
    }
}
