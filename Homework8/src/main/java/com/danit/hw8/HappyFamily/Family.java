package com.danit.hw8.HappyFamily;

import com.danit.hw8.HappyFamily.Humans.Human;
import com.danit.hw8.HappyFamily.Pets.Pet;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pet;

    public Family() {}

    public Family(Human mother, Human father) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    public List<Human> addChild(Human human) {
        children.add(human);

        return children;
    }

    public boolean removeChild(int index) {
        if (index > children.size() || index < 0) {
            return false;
        } else {
            children.remove(index);

            return true;
        }
    }

    public boolean removeChild(Human human) {
        if (!children.contains(human)) return false;
        children.remove(human);

        return true;
    }

    public int countFamily() {
        int familySize = 0;

        if (children != null) {
            familySize = 2 + children.size();
        }

        return familySize;
    }

    public String toJson() {
        String result = "";
        String kidsPrettier = children == null ? "" : "\n\t\t\t";
        String petsPrettier = pet == null ? "" : "\n\t\t\t";

        if (children != null)
            for (int i = 0; i < children.size(); i++) {
                String gender = children.get(i).getClass().getName().contains("Man") ?
                        "\"boy\"" : children.get(i).getClass().getName().contains("Woman") ?
                        "\"girl\"" : "\"unknown\"";
                kidsPrettier += "{" + gender + ": {" + children.get(i).toJson() + "}" + "}" + (i == children.size() - 1 ? "" : ",") + "\n\t\t\t";
            }

        int petCounter = 0;
        if (pet != null)
            for (Pet p : pet) {
                petsPrettier += p.toJson() + (petCounter == pet.size() - 1 ? "" : ",") + "\n\t\t\t";
                ++petCounter;
            }

        result = String.format("{%n \"Mother\" : { %s },%n \"Father\" : { %s },%n      \"Children\":  [%s], %n        \"Pet\":  [%s]}",
                mother.toJson(), father.toJson(), kidsPrettier, petsPrettier);

        return result;
    }

    public String prettyFormat() {
        String result = "";
        String kidsPrettier = children == null ? "no children" : "\n\t\t\t";
        String petsPrettier = pet == null ? "no pets" : "\n\t\t\t";

        if (children != null)
        for (Human child : children) {
            String gender = child.getClass().getName().contains("Man") ?
                    "boy" : child.getClass().getName().contains("Woman") ?
                    "girl" : "unknown";
            kidsPrettier += gender + ": " + child + "\n\t\t\t";
        }

        if (pet != null)
        for (Pet p : pet) {
            petsPrettier += p + "\n\t\t\t";
        }

        result = String.format("FAMILY:%n Mother = { %s },%n Father = { %s },%n      Children:  %s %n        Pet:  %s",
                mother.toString(), father.toString(), kidsPrettier, petsPrettier);

        return result;
    }


    @Override
    public String toString() {
        String kids = children == null ? "no children" : children.toString();
        String pets = pet == null ? "no pets" : pet.toString();

        return String.format("Mother = { %s }, Father = { %s }, Children = { %s },  Pet = { %s }",
                mother.toString(), father.toString(), kids, pets);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(getMother(), family.getMother()) &&
                Objects.equals(getFather(), family.getFather()) &&
                Objects.equals(getChildren(), family.getChildren()) &&
                Objects.equals(getPet(), family.getPet());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMother(), getFather(), getChildren(), getPet());
    }

    @Override
    protected void finalize() {
        System.out.printf("This object removed. Mother = { %s }, Father = { %s }, Children = { %s },  Pet = { %s }",
                mother.toString(), father.toString(), children.toString(), pet.toString() );
        System.out.println();
    }
}
