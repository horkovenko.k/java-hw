package com.danit.hw8.HappyFamily.Pets;

import com.danit.hw8.Enums.Species;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Set;

@JsonDeserialize(as=Fish.class)
public class Fish extends Pet {
    {species = Species.Fish;}


    public Fish() {super();}

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age) {
        super(nickname, age);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("byl' byl' byl'");
    }
}