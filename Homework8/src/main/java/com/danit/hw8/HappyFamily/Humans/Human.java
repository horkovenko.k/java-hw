package com.danit.hw8.HappyFamily.Humans;

import com.danit.hw8.HappyFamily.Family;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

public class Human {
    protected String name;
    protected String surname;
    protected int iq;
    protected Map<String, String> schedule;
    protected Family family;

    protected long unixMillisTimestamp;
    protected static SimpleDateFormat datePattern = new SimpleDateFormat("dd/MM/yyyy");

    public Human() {}

    public Human(String name, String surname, String birthDate) throws ParseException {
        this.name = name;
        this.surname = surname;
        unixMillisTimestamp = setUnixMs(birthDate);
    }

    public Human(String name, String surname, String birthDate, int iq) throws ParseException {
        this.name = name;
        this.surname = surname;

        unixMillisTimestamp = setUnixMs(birthDate);

        this.iq = iq;
    }

    public Human(String name, String surname, String birthDate, int iq, Map<String, String> schedule) throws ParseException {
        this.name = name;
        this.surname = surname;

        unixMillisTimestamp = setUnixMs(birthDate);

        this.iq = iq;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getUnixMillisTimestamp() {
        return unixMillisTimestamp;
    }

    public void setUnixMillisTimestamp(long unixMillisTimestamp) {
        this.unixMillisTimestamp = unixMillisTimestamp;
    }

    public int getBirthdayYear() {
        int year = LocalDate.now().getYear();

        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(getUnixMillisTimestamp());

        return year - calendar.getWeekYear();
    }

    public void setBirthday(String birthDate) throws ParseException {
        unixMillisTimestamp = setUnixMs(birthDate);
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public void greetPet() {
        System.out.format("Привет, %s", family.getPet().toString());
    }

    public void describePet() {
        System.out.format("У меня есть %s", family.getPet().toString());
    }

    private long setUnixMs(String date) throws ParseException {
        return datePattern.parse(date).getTime();
    }

    public String describeAge() {
        String res = "";

        LocalDate d = Instant.ofEpochMilli(this.unixMillisTimestamp)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        int year = LocalDate.now().getYear();

        res = String.format("I lived for %s years, %s months and %s days",
                                        Integer.toString(year - d.getYear()),
                                        Integer.toString(d.getMonth().getValue()),
                                        Integer.toString(d.getDayOfMonth()) );

        return res;
    }

    @Override
    public String toString() {
        String isScheduleNull = schedule == null ? "no schedule" : schedule.toString();

        LocalDate d = Instant.ofEpochMilli(this.unixMillisTimestamp)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        String bDate = String.format("%s/%s/%s", d.getDayOfMonth(), d.getMonth().getValue(), d.getYear());

        return String.format("Name = %s, Surname = %s, Birthday = %s, iq = %d, schedule = %s",
                name, surname, bDate, iq, isScheduleNull );
    }

    private String mapToJson() {
        String result = "";

        if (schedule != null)
        for (Map.Entry<String, String> pair : schedule.entrySet()) {
            result += "{\"" + pair.getKey() + "\"" + ":" + "\"" + pair.getValue() + "\"}" ;
        }

        String s = result.replaceAll("}\\{", "},{");

        return "[" + s + "]";
    }

    public String toJson() {
        String isScheduleNull = schedule == null ? "" : schedule.toString();

        LocalDate d = Instant.ofEpochMilli(this.unixMillisTimestamp)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        String bDate = String.format("%s/%s/%s", d.getDayOfMonth(), d.getMonth().getValue(), d.getYear());

        return String.format("\"Name\" : \"%s\", \"Surname\" : \"%s\", \"Birthday\" : \"%s\", \"iq\" : \"%d\", \"schedule\" : %s",
                name, surname, bDate, iq, mapToJson() );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getIq() == human.getIq() &&
                getUnixMillisTimestamp() == human.getUnixMillisTimestamp() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname()) &&
                Objects.equals(getSchedule(), human.getSchedule()) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getIq(), getSchedule(), family, getUnixMillisTimestamp());
    }

    @Override
    protected void finalize() {
        LocalDate d = Instant.ofEpochMilli(this.unixMillisTimestamp)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        String bDate = String.format("%s/%s/%s", d.getDayOfMonth(), d.getMonth().getValue(), d.getYear());

        System.out.printf("This object removed. Name = %s, Surname = %s, Birthday = %s, iq = %d, schedule = %s",
                name, surname, bDate, iq, schedule.toString() );
        System.out.println();
    }

    public void setFamily(Family family) {
        this.family = family;
    }
}