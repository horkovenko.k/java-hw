package com.danit.hw8.HappyFamily.Pets;

import com.danit.hw8.Enums.Species;
import com.danit.hw8.Interfaces.BadHabits;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Set;

@JsonDeserialize(as=Dog.class)
public class Dog extends Pet implements BadHabits {
    {species = Species.Dog;}

    Dog() {super();}

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age) {
        super(nickname, age);
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("bark bark");
    }

    @Override
    public void foul() {
        System.out.println("Dirty dids had been done");
    }
}
