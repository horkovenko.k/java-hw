package com.danit.hw8.HappyFamily.Pets;

import com.danit.hw8.Enums.Species;
import com.danit.hw8.Interfaces.BadHabits;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Set;

@JsonDeserialize(as=DomesticCat.class)
public class DomesticCat extends Pet implements BadHabits {
    {species = Species.DomesticCat;}

    DomesticCat() {super();}

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age) {
        super(nickname, age);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("meow meow");
    }

    @Override
    public void foul() {
        System.out.println("Try to find my footprints");
    }
}
