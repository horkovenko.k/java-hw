package com.danit.hw8.HappyFamily.Humans;

import java.text.ParseException;
import java.util.Map;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, String birthDate) throws ParseException {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, int iq) throws ParseException {
        super(name, surname, birthDate, iq);
    }

    public Man(String name, String surname, String birthDate, int iq, Map<String, String> schedule) throws ParseException {
        super(name, surname, birthDate, iq, schedule);
    }

    public void repairCar() {
        System.out.println("Repairing Car Now!");
    }

    @Override
    public void greetPet() {
        System.out.printf("Hello Pet, %s! Here is your owner %s", family.getPet(), name);
        System.out.println();
    }
}
