package com.danit.hw8.HappyFamily.Pets;

import com.danit.hw8.Enums.Species;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Set;

@JsonDeserialize(as=RoboCat.class)
public class RoboCat extends Pet {
    {species = Species.RoboCat;}

    RoboCat() {super();}

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age) {
        super(nickname, age);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Hello! I am Robo Cat");
    }
}

