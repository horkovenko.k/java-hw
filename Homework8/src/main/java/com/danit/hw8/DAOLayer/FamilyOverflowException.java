package com.danit.hw8.DAOLayer;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException(String errorMessage) {
        super(errorMessage);
    }
}
