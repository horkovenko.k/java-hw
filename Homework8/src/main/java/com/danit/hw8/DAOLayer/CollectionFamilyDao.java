package com.danit.hw8.DAOLayer;

import com.danit.hw8.HappyFamily.Family;
import com.danit.hw8.Interfaces.FamilyDao;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families = new ArrayList<Family>();

    public List<Family> getFamilies() {
        return families;
    }

    public void setFamilies(List<Family> families) {
        this.families = families;
    }

    @Override
    public List<Family> getAllFamilies() {

        families.forEach(Family::prettyFormat);

        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index < 0 || index > families.size()) return null;

        return families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index < 0 || index > families.size()) return false;
        families.remove(index);

        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (!families.contains(family)) return false;
        deleteFamily(families.indexOf(family));

        return true;
    }

    @Override
    public List<Family> saveFamily(Family family) {

        int index = families.indexOf(family);

        if (index == -1) {
            families.add(family);
        } else {
            families.set(index, family);
        }

        return families;
    }

    @Override
    public void loadData(List<Family> families) throws IOException {
        File file = new File("src\\main\\java\\com\\danit\\hw8\\data.txt");

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(file, families);
    }

    @Override
    public String getData() {
        StringBuilder sb = new StringBuilder();
        File file = new File("src\\main\\java\\com\\danit\\hw8\\data.txt");

        try {
            try (BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
                String s;
                while ((s = in.readLine()) != null) {
                    sb.append(s);
                    sb.append("\n");
                }
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        }

        return sb.toString();
    }
}
