package com.danit.hw8.Enums;

public enum Species  {
    Fish,
    DomesticCat,
    Dog,
    RoboCat,
    UNKNOWN
}