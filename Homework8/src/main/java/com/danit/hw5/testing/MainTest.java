package com.danit.hw5.testing;

import com.danit.hw5.HappyFamily.Family;
import com.danit.hw5.HappyFamily.Human;
import com.danit.hw5.HappyFamily.Pet;
import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void toStringTest() {
        Pet pet = new Pet();
        Human human = new Human();
        Human[] kids = new Human[]{human};
        Family family = new Family(human, human, kids, pet);

        boolean isPetContainSpecies = pet.toString().contains("Species");
        boolean isHumanContainScheduleAndBirthday= human.toString().contains("Birthday") && human.toString().contains("schedule");
        boolean isFamilyContainMotherAndFather= family.toString().contains("Mother") && family.toString().contains("Father");

        Assert.assertTrue("Pet toString method must contain Species", isPetContainSpecies);
        Assert.assertTrue("Human toString method must contain Birthday, schedule", isHumanContainScheduleAndBirthday);
        Assert.assertTrue("Family toString method must contain Mother, Father", isFamilyContainMotherAndFather);
    }
}
