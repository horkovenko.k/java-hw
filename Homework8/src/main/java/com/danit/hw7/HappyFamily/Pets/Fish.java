package com.danit.hw7.HappyFamily.Pets;

import java.util.Set;

public class Fish extends Pet {
    public Fish() {}

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age) {
        super(nickname, age);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("byl' byl' byl'");
    }
}