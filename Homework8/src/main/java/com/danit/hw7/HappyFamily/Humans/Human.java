package com.danit.hw7.HappyFamily.Humans;

import com.danit.hw7.HappyFamily.Family;
import com.danit.hw7.HappyFamily.Pets.Pet;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

public class Human {
    protected String name;
    protected String surname;
    protected int birthday;
    protected int iq;
    protected Map<String, String> schedule;
    protected Family family;

    public Human() {}

    public Human(String name, String surname, int birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public Human(String name, String surname, int birthday, int iq, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.iq = iq;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getBirthday() {
        return birthday;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public void greetPet() {
        System.out.format("Привет, %s", family.getPet().toString());
    }

    public void describePet() {
        System.out.format("У меня есть %s", family.getPet().toString());
    }

    @Override
    public String toString() {
        return String.format("Name = %s, Surname = %s, Birthday = %d, iq = %d, schedule = %s",
                name, surname, birthday, iq, schedule.toString() );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getBirthday() == human.getBirthday() &&
                getIq() == human.getIq() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname()) &&
                Objects.equals(getSchedule(), human.getSchedule()) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getBirthday(), getIq(), getSchedule(), family);
    }

    @Override
    protected void finalize() {
        System.out.printf("This object removed. Name = %s, Surname = %s, Birthday = %d, iq = %d, schedule = %s",
                name, surname, birthday, iq, schedule.toString() );
        System.out.println();
    }

    public void setFamily(Family family) {
        this.family = family;
    }
}