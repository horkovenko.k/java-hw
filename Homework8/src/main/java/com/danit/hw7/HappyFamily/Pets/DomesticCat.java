package com.danit.hw7.HappyFamily.Pets;

import com.danit.hw7.Interfaces.BadHabits;

import java.util.Set;

public class DomesticCat extends Pet implements BadHabits {
    DomesticCat() {}

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age) {
        super(nickname, age);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("meow meow");
    }

    @Override
    public void foul() {
        System.out.println("Try to find my footprints");
    }
}
