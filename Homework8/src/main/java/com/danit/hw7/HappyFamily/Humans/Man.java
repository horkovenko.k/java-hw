package com.danit.hw7.HappyFamily.Humans;

import com.danit.hw7.HappyFamily.Pets.Pet;

import java.util.Map;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, int birthday) {
        super(name, surname, birthday);
    }

    public Man(String name, String surname, int birthday, int iq, Map<String, String> schedule) {
        super(name, surname, birthday, iq, schedule);
    }

    public void repairCar() {
        System.out.println("Repairing Car Now!");
    }

    @Override
    public void greetPet() {
        System.out.printf("Hello Pet, %s! Here is your owner %s", family.getPet(), name);
        System.out.println();
    }
}
