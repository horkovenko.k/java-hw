package com.danit.hw7.HappyFamily;

import com.danit.hw7.HappyFamily.Humans.Human;
import com.danit.hw7.HappyFamily.Pets.Pet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

public class Family {
    private Human mother;
    private Human father;
    private ArrayList<Human> children;
    private Set<Pet> pet;

    public Family(Human mother, Human father) {
        mother.setFamily(this);
        father.setFamily(this);
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    public ArrayList<Human> addChild(Human human) {
        children.add(human);

        return children;
    }

    public boolean removeChild(int index) {
        if (index > children.size() || index < 0) {
            return false;
        } else {
            children.remove(index);

            return true;
        }
    }

    public boolean removeChild(Human human) {
        if (!children.contains(human)) return false;
        children.remove(human);

        return true;
    }

    public int countFamily() {
        System.out.println(2 + children.size());
        return 2 + children.size();
    }

    @Override
    public String toString() {
        return String.format("Mother = { %s }, Father = { %s }, Children = { %s },  Pet = { %s }",
                mother.toString(), father.toString(), children.toString(), pet.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(getMother(), family.getMother()) &&
                Objects.equals(getFather(), family.getFather()) &&
                Objects.equals(getChildren(), family.getChildren()) &&
                Objects.equals(getPet(), family.getPet());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMother(), getFather(), getChildren(), getPet());
    }

    @Override
    protected void finalize() {
        System.out.printf("This object removed. Mother = { %s }, Father = { %s }, Children = { %s },  Pet = { %s }",
                mother.toString(), father.toString(), children.toString(), pet.toString() );
        System.out.println();
    }
}
