package com.danit.hw7.HappyFamily;

import com.danit.hw7.Enums.DayOfWeek;
import com.danit.hw7.Enums.Species;
import com.danit.hw7.HappyFamily.Humans.Human;
import com.danit.hw7.HappyFamily.Humans.Man;
import com.danit.hw7.HappyFamily.Humans.Woman;
import com.danit.hw7.HappyFamily.Pets.*;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        Set<String> habits = new HashSet<String>();
        habits.add("swim");
        habits.add("eat");
        Fish pet = new Fish("gold", 1, 12, habits);

        pet.setSpecies(Species.Fish);
        DayOfWeek Monday = DayOfWeek.MONDAY;
        DayOfWeek Tuesday = DayOfWeek.TUESDAY;
        DayOfWeek Wednesday = DayOfWeek.WEDNESDAY;
        DayOfWeek Thursday = DayOfWeek.THURSDAY;
        DayOfWeek Friday = DayOfWeek.FRIDAY;
        DayOfWeek Saturday = DayOfWeek.SATURDAY;
        DayOfWeek Sunday = DayOfWeek.SUNDAY;

        Map<String, String> schedule = new HashMap<String, String>();

        schedule.put(Monday.name(), "task1");
        schedule.put(Tuesday.name(), "task2");
        schedule.put(Wednesday.name(), "task3");
        schedule.put(Thursday.name(), "task4");
        schedule.put(Friday.name(), "task5");
        schedule.put(Saturday.name(), "task6");
        schedule.put(Sunday.name(), "task7");

        // 7 Homework
        System.out.println(pet);

        Human h = new Human("Name", "Surname", 1800, 199, schedule);

        Human h1 = new Human("Name1", "Surname", 1800, 199, schedule);
        Human h2 = new Human("Name2", "Surname", 1800, 199, schedule);

        ArrayList<Human> kids = new ArrayList<Human>();

        kids.add(h);
        kids.add(h);
        kids.add(h);

        Set<Pet> pets = new HashSet<Pet>();
        pets.add(pet);

        Family f = new Family(h1, h2);

        f.setPet(pets);
        f.setChildren(kids);

        f.removeChild(2);
        System.out.println(f);

        h.setFamily(f);
        h.describePet();
        System.out.println(h);

        Woman m = new Woman("moto", "mech", 1111, 11, schedule);
        m.setFamily(f);

        m.greetPet();

    }

}
