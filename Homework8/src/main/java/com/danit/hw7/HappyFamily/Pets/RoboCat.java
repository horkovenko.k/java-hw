package com.danit.hw7.HappyFamily.Pets;

import java.util.Set;

public class RoboCat extends Pet {
    RoboCat() {}

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age) {
        super(nickname, age);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Hello! I am Robo Cat");
    }
}

