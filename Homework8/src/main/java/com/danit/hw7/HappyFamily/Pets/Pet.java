package com.danit.hw7.HappyFamily.Pets;

import com.danit.hw7.Enums.Species;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

public abstract class Pet {

    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

    public Pet() {
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age) {
        this.nickname = nickname;
        this.age = age;
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }

    public abstract void respond();

    @Override
    public String toString() {
        return String.format("Nickname = %s, Species = %s, Age = %d, Trick level = %d, Habits = %s ",
                nickname, species, age, trickLevel, habits.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;
        Pet pet = (Pet) o;
        return getAge() == pet.getAge() &&
                getTrickLevel() == pet.getTrickLevel() &&
                getSpecies() == pet.getSpecies() &&
                Objects.equals(getNickname(), pet.getNickname()) &&
                Objects.equals(getHabits(), pet.getHabits());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSpecies(), getNickname(), getAge(), getTrickLevel(), getHabits());
    }

    @Override
    protected void finalize() {
        System.out.printf("This object removed. Nickname = %s, Species = %s, Age = %d, Trick level = %d, Habits = %s ",
                nickname, species, age, trickLevel, habits.toString());
        System.out.println();
    }

}