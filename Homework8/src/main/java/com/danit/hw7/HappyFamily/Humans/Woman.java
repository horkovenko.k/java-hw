package com.danit.hw7.HappyFamily.Humans;

import com.danit.hw7.HappyFamily.Pets.Pet;

import java.util.Map;

public final class Woman extends Human {
    public Woman() {
    }

    public Woman(String name, String surname, int birthday) {
        super(name, surname, birthday);
    }

    public Woman(String name, String surname, int birthday, int iq, Map<String, String> schedule) {
        super(name, surname, birthday, iq, schedule);
    }

    public void makeUp() {
        System.out.println("Making Up Now!");
    }

    @Override
    public void greetPet() {
        System.out.printf("Hello my dear, %s! I am %s", family.getPet(), name);
        System.out.println();
    }
}
