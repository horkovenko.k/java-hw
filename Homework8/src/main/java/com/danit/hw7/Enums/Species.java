package com.danit.hw7.Enums;

public enum Species  {
    Fish,
    DomesticCat,
    Dog,
    RoboCat,
    UNKNOWN
}