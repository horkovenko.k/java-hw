package com.danit.hw6.Enums;

public enum Species  {
    Fish,
    DomesticCat,
    Dog,
    RoboCat,
    UNKNOWN
}