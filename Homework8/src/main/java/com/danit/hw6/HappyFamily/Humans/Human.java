package com.danit.hw6.HappyFamily.Humans;

import com.danit.hw6.HappyFamily.Pets.Pet;

import java.util.Arrays;
import java.util.Objects;

public class Human {
    protected String name;
    protected String surname;
    protected int birthday;
    protected int iq;
    protected String[][] schedule;
    protected Pet pet;

    public Human() {}

    public Human(String name, String surname, int birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public Human(String name, String surname, int birthday, int iq, String[][] schedule, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.iq = iq;
        this.schedule = schedule;
        this.pet = pet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getBirthday() {
        return birthday;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void greetPet() {
        System.out.format("Привет, %s", pet.getNickname());
    }

    public void describePet() {
        String trick = "почти не хитрый";
        if (pet.getTrickLevel() > 50) {
            trick = "очень хитрый";
        }

        System.out.format("У меня есть %s, ему %d лет, он %s", pet.getNickname(), pet.getAge(), trick);
    }

    @Override
    public String toString() {
        return String.format("Name = %s, Surname = %s, Birthday = %d, iq = %d, schedule = %s",
                name, surname, birthday, iq, Arrays.deepToString(schedule) );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return getBirthday() == human.getBirthday() &&
                getIq() == human.getIq() &&
                Objects.equals(getName(), human.getName()) &&
                Objects.equals(getSurname(), human.getSurname()) &&
                Arrays.equals(getSchedule(), human.getSchedule());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getName(), getSurname(), getBirthday(), getIq());
        result = 31 * result + Arrays.hashCode(getSchedule());
        return result;
    }

    @Override
    protected void finalize() {
        System.out.printf("This object removed. Name = %s, Surname = %s, Birthday = %d, iq = %d, schedule = %s",
                name, surname, birthday, iq, Arrays.deepToString(schedule) );
        System.out.println();
    }
}