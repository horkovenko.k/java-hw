package com.danit.hw6.HappyFamily.Pets;

public class Fish extends Pet {
    public Fish() {}

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age) {
        super(nickname, age);
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("byl' byl' byl'");
    }
}