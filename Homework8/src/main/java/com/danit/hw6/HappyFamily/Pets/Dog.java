package com.danit.hw6.HappyFamily.Pets;

import com.danit.hw6.Interfaces.BadHabits;

public class Dog extends Pet implements BadHabits {
    Dog() {}

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age) {
        super(nickname, age);
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("bark bark");
    }

    @Override
    public void foul() {
        System.out.println("Dirty dids had been done");
    }
}
