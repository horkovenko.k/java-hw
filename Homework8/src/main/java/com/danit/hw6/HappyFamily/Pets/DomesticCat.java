package com.danit.hw6.HappyFamily.Pets;

import com.danit.hw6.Interfaces.BadHabits;

public class DomesticCat extends Pet implements BadHabits {
    DomesticCat() {}

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age) {
        super(nickname, age);
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("meow meow");
    }

    @Override
    public void foul() {
        System.out.println("Try to find my footprints");
    }
}
