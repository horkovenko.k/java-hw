package com.danit.hw6.testing;

import com.danit.hw6.HappyFamily.Family;
import com.danit.hw6.HappyFamily.Humans.Human;
import com.danit.hw6.HappyFamily.Pets.Fish;
import com.danit.hw6.HappyFamily.Pets.Pet;
import org.junit.Assert;
import org.junit.Test;

public class FamilyTest {
    private Human h1 = new Human("n1", "s1", 1);
    private Human h3 = new Human("n3", "s3", 1);
    private Human h2 = new Human("n2", "s2", 1);
    private Human h4 = new Human("n4", "s4", 1);
    private Human h5 = new Human("n5", "s5", 1);
    private Human[] kids = new Human[]{h3, h4, h5};
    private Human[] kidsAddTest = new Human[]{h3, h4, h5, h3};
    private Human[] kidsRemoveTest = new Human[]{h4, h5};
    private Fish p1 = new Fish();

    private Family f = new Family(h1, h2, kids, p1);

    @Test
    public void removeChildTestIndex() {
        f.removeChild(0);
        f.removeChild(3); // index out of bounds
        f.removeChild(-3); // index out of bounds
        Assert.assertArrayEquals(kidsRemoveTest, f.getChildren());
    }

    @Test
    public void removeChildTestObject() {
        f.removeChild(h3);
        f.removeChild(h1); // index out of bounds
        f.removeChild(h2); // index out of bounds
        Assert.assertArrayEquals(kidsRemoveTest, f.getChildren());
    }

    @Test
    public void addChildTest() {
        f.addChild(h3);

        boolean isAddedChildCorrect = h3.equals(f.getChildren()[3]);

        Assert.assertArrayEquals(kidsAddTest, f.getChildren());
        Assert.assertEquals(h3.toString(), f.getChildren()[3].toString());
        Assert.assertSame(h3, f.getChildren()[3]);
    }


    @Test
    public void countFamily() {
        Assert.assertEquals(5, f.countFamily());
        f.removeChild(0);
        Assert.assertEquals(4, f.countFamily());
        f.addChild(h5);
        Assert.assertEquals(5, f.countFamily());
    }

}
